#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/8/14 上午10:37
# @Author  : 冷月孤心 && AnJia
# @Mail    : codenutter@foxmail.com && anjia0532@gmail.com
# @File    : wechat_alert.py
#! /usr/bin/env python
# -*- coding: utf-8 -*-

import json
import redis
import datetime
from elastalert.alerts import Alerter
import requests
import sys
import warnings
reload(sys)
sys.setdefaultencoding('utf-8')
warnings.filterwarnings("ignore")  # 去掉所有运行时警告输出

class WeChatAlerter(Alerter):

    #企业号id，secret，应用id必填

    required_options = frozenset(['corp_id','secret','agent_id'])

    def __init__(self, *args):
        super(WeChatAlerter, self).__init__(*args)
        self.corp_id = self.rule.get('corp_id', '')     #企业号id
        self.secret = self.rule.get('secret', '')       #secret
        self.agent_id = self.rule.get('agent_id', '')   #应用id

        self.party_id = self.rule.get('party_id')       #部门id
        self.user_id = self.rule.get('user_id', '')     #用户id，多人用 | 分割，全部用 @all
        self.tag_id = self.rule.get('tag_id', '')       #标签id
        self.access_token = ''                          #微信身份令牌
        self.redis_host = self.rule.get('redis_host', 'localhost')  # redis主机
        self.redis_port = self.rule.get('redis_port', 6379)  # redis端口号
        self.log_path = self.rule.get('log_path', )   # 程序执行log输出文件

    def set_redis_value(self, key, value, extime):
        #  使用连接池提高程序执行效率
        pool = redis.ConnectionPool(host=self.redis_host, port=self.redis_port)
        r = redis.Redis(connection_pool=pool)
        r.set(key, value, extime)

    def get_redis_value(self, key):
        #  使用连接池提高程序执行效率
        pool = redis.ConnectionPool(host=self.redis_host, port=self.redis_port)
        r = redis.Redis(connection_pool=pool)
        self.access_token = r.get(key)

    def write_log(self, record):
        # 由于和ElastAlert结合起来logging貌似失效了，因此写个比较low的日志记录器
        with open(self.log_path, 'a+') as f:
            f.write(record + '\n')

    def alert(self, matches):
        # 参考elastalert的写法
        # https://github.com/Yelp/elastalert/blob/master/elastalert/alerts.py#L236-L243
        body = self.create_alert_body(matches)

        # 微信企业号获取Token文档
        # http://qydev.weixin.qq.com/wiki/index.php?title=AccessToken
        self.get_token()
        self.senddata(body)

    def get_token(self):
        # 先从redis请求token
        self.get_redis_value('access_token')
        if self.access_token:
            self.write_log("%s|INFO|Token OK|From redis|%s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), self.access_token))
            return

        # reids没有token则发送http请求token，并重新缓存
        #构建获取token的url
        get_token_url = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s' %(self.corp_id, self.secret)
        try:
            response = requests.get(get_token_url, verify=False)
        except Exception as e:
            self.write_log("%s|ERROR|Toenk Error|From http|%s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), e.message))
            return
        token_json = response.json()

        if 'access_token' in token_json :
            self.write_log("%s|INFO|Token OK|From http|%s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), response.text))
            self.access_token = token_json['access_token']
            self.set_redis_value('access_token', self.access_token, 7100)
        else:
            self.write_log("%s|ERROR|Token Error|From http:%s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), response.text))

    def senddata(self, content):
        #如果需要原始json，需要传入matches
        # 微信企业号有字符长度限制（2048），超长自动截断
        # 参考 http://blog.csdn.net/handsomekang/article/details/9397025
        # len utf8 3字节，gbk2 字节，ascii 1字节
        if len(content) > 2048:
            content = content[:2045] + "..."

        send_url = 'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s' % self.access_token
        headers = {'content-type': 'application/json'}

        #最新微信企业号调整校验规则，tagid必须是string类型，如果是数字类型会报错，故而使用str()函数进行转换
        payload = {
            "touser": self.user_id and str(self.user_id) or '', #用户账户，建议使用tag
            "toparty": self.party_id and str(self.party_id) or '', #部门id，建议使用tag
            "totag": self.tag_id and str(self.tag_id) or '', #tag可以很灵活的控制发送群体细粒度。比较理想的推送应该是，在heartbeat或者其他elastic工具自定义字段，添加标签id。这边根据自定义的标签id，进行推送
            'msgtype': "text",
            "agentid": self.agent_id,
            "text":{
                "content": content.encode('UTF-8') #避免中文字符发送失败
               },
            "safe":"0"
        }

        # set https proxy, if it was provided
        # 如果需要设置代理，可修改此参数并传入requests
        # proxies = {'https': self.pagerduty_proxy} if self.pagerduty_proxy else None
        try:
            response = requests.post(send_url, data=json.dumps(payload, ensure_ascii=False), headers=headers, verify=False)
            self.write_log("%s|INFO|Send OK|%s|%s|%s|%s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), self.user_id, self.party_id, content, response.text))
        except Exception as e:
            self.write_log("%s|ERROR|Send Error|%s|%s|%s|%s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), self.user_id, self.party_id, content, e.message))

    def get_info(self):
        return {'type': 'WeChatAlerter'}
